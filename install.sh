#!/bin/sh

curl https://gitlab.com/ramos.vitor89/pascal-releases/-/raw/main/pascalanalyzer --output pascalanalyzer
curl https://gitlab.com/ramos.vitor89/pascal-releases/-/raw/main/libmanualinst.so --output libmanualinst.so
curl https://gitlab.com/ramos.vitor89/pascal-releases/-/raw/main/libautoinst.so --output libautoinst.so

chmod +x pascalanalyzer

cp libautoinst.so /usr/lib/
cp libmanualinst.so /usr/lib/
cp pascalanalyzer /bin